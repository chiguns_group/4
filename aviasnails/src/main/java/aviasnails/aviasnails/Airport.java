package aviasnails.aviasnails;

public class Airport {
    City city;
    String name;

    public Airport(String name, City city)
    {
        this.name = name;
        this.city = city;
    }
}
