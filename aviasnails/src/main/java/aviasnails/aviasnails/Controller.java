package aviasnails.aviasnails;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@org.springframework.stereotype.Controller
public class Controller {

    @GetMapping(value = "/")
    public String home(Model model,
                       HttpServletRequest request,
                       @CookieValue(value = "email",
                               required = false,
                               defaultValue = "-") String userEmail) {
        FlightsBase base = FlightsBase.getBase();
        userEmail = userEmail.equals("-") ? "Не авторизированы" : userEmail;
        Set<String> cityStartSet = new HashSet<>();
        Set<String> cityEndSet = new HashSet<>();
        for (int i = 0; i < base.flights.size(); ++i)
        {
            cityStartSet.add(base.flights.get(i).airportStart.city.name);
            cityEndSet.add(base.flights.get(i).airportEnd.city.name);
        }
        model.addAttribute("cityStart", cityStartSet.toArray());
        model.addAttribute("cityEnd", cityEndSet.toArray());
        model.addAttribute("userEmail", userEmail);

        List<Dictionary<String, String>> flights = new ArrayList<>();
        for (int i = 0; i < base.flights.size(); ++i)
        {
            Flight curr = base.flights.get(i);
            Dictionary<String, String> flight = new Hashtable<>();
                flight.put("text",
                        "Из " + curr.airportStart.name + " в " +
                                curr.airportEnd.name + " на дату " +
                                curr.date + " по цене за эконом: " +
                                curr.costEconom);
            flight.put("id", Integer.toString(curr.id));
            flights.add(flight);
        }
        model.addAttribute("flights", flights);

        return "home";
    }

    @PostMapping(value = "/buy")
    public String buy(Model model,
                      @RequestParam("cityStart") String cityStart,
                      @RequestParam("cityEnd") String cityEnd,
                      @CookieValue(value = "email",
                              required = false,
                              defaultValue = "-") String userEmail,
                      HttpServletRequest request) {
        List<Dictionary<String, String>> flights = new ArrayList<>();
        FlightsBase base = FlightsBase.getBase();
        userEmail = userEmail.equals("-") ? "Не авторизированы" : userEmail;
        for (int i = 0; i < base.flights.size(); ++i) {
            Flight curr = base.flights.get(i);
            if (curr.airportStart.city.name.equals(cityStart) &&
                    curr.airportEnd.city.name.equals(cityEnd)) {
                Dictionary<String, String> flight = new Hashtable<>();
                flight.put("text",
                        "Из " + base.flights.get(i).airportStart.name + " в " +
                                base.flights.get(i).airportEnd.name + " на дату " +
                                base.flights.get(i).date + " по цене за эконом: " +
                                base.flights.get(i).costEconom);
                flight.put("id", Integer.toString(base.flights.get(i).id));
                flights.add(flight);
            }
        }
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("flights", flights);
        model.addAttribute("route", "Из " + cityStart + " в " + cityEnd);
        return "buy";
    }

    @GetMapping(value = "/profile")
    public String profileGET(Model model,
                             @CookieValue(value = "email",
                                     required = false,
                                     defaultValue = "-") String userEmail,
                             HttpServletRequest request)
    {
        FlightsBase base = FlightsBase.getBase();
        if (base.users.get(userEmail) == null)
        {
            return "registration";
        }
        else
        {
            model.addAttribute("userEmail", userEmail);
            model.addAttribute("flights", base.users.get(userEmail).myFlights);
            return "profile";
        }
    }

    @PostMapping(value = "/profile")
    public String profile(Model model,
                          @RequestParam(value = "flightId", required = false, defaultValue = "-1") String id,
                          @CookieValue(value = "email",
                                  required = false,
                                  defaultValue = "-") String userEmail,
                          HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        FlightsBase base = FlightsBase.getBase();
        if (base.users.get(userEmail) == null)
        {
            return "registration";
        }
        else {
            if (!id.equals("-1")) {
                base.users.get(userEmail).myFlights.add(base.flights.get(Integer.parseInt(id)));
            }
            model.addAttribute("userEmail", userEmail);
            userEmail = userEmail.equals("-") ? "Не авторизированы" : userEmail;
            model.addAttribute("flights", base.users.get(userEmail).myFlights);
            return "profile";
        }
    }

    @GetMapping(value = "/registration")
    public String registration(Model model,
                               HttpServletRequest request) {
        return "registration";
    }

    @GetMapping(value = "/login")
    public String logIn(Model model,
                        HttpServletRequest request) {
        return "login";
    }

    @PostMapping(value = "/authorise")
    public String authorise(HttpServletResponse response, Model model,
                            @RequestParam(value = "email") String email,
                            @RequestParam(value = "password") String password,
                            @RequestParam(value = "act") String act,
                            HttpServletRequest request)
    {
        String status = "Что-то пошло не так...";
        FlightsBase base = FlightsBase.getBase();
        User currUser = new User(email, password);
        if (act.equals("log in"))
        {
            if (base.users.get(currUser.email) != null) {
                response.addCookie(new Cookie("email", email));
                status = "Успешно вошли";
            }
        }
        else if (act.equals("register"))
        {
            if (base.users.get(currUser.email) == null) {
                base.users.put(currUser.email, currUser);
                response.addCookie(new Cookie("email", email));
                status = "Успешно зарегистрировались";
            }
        }
        model.addAttribute("status", status);
        return "authorised";
    }
    @GetMapping(value = "/cancel")
    public String cancel(Model model,
                        HttpServletRequest request) {
        return "cancel";
    }
}