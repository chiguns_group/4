package aviasnails.aviasnails;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public class FlightsBase {
    public List<Flight> flights = new ArrayList<>();
    public List<City> cities = new ArrayList<>();
    public List<Airport> airports = new ArrayList<>();
    public Dictionary<String, User> users = new Hashtable<>();
    private static FlightsBase mainBase = null;

    public static FlightsBase getBase()
    {
        if(mainBase == null)
        {
            ApplicationContext context = new
                    AnnotationConfigApplicationContext(BeanConfig.class);
            mainBase = context.getBean(FlightsBase.class);
        }
        return mainBase;
    }

    public FlightsBase()
    {
        cities.add(new City("Москва"));
        cities.add(new City("Воркута"));
        cities.add(new City("Магадан"));
        cities.add(new City("Челябинск"));
        airports.add(new Airport("Сокол", cities.get(2)));
        airports.add(new Airport("Шереметьево", cities.get(0)));
        airports.add(new Airport("Внуково", cities.get(0)));
        airports.add(new Airport("Воркута", cities.get(1)));
        airports.add(new Airport("Им. Курчатова", cities.get(3)));
        flights.add(new Flight(0, airports.get(0), airports.get(1)));
        flights.add(new Flight(1, airports.get(1), airports.get(3)));
    }
}
