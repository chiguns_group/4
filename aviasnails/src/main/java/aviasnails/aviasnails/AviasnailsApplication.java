package aviasnails.aviasnails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AviasnailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AviasnailsApplication.class, args);
	}

}
